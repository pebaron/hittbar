/// a simple testing macro for the MuonSelectorTools_xAOD package
/// shamelessly stolen from CPToolTests.cxx

// System include(s):
#include <memory>
#include <cstdlib>
#include <string>
#include <map>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TStopwatch.h>
#include <TString.h>
#include <TLorentzVector.h>

// Infrastructure include(s):
#ifdef ROOTCORE
#   include "xAODRootAccess/Init.h"
#   include "xAODRootAccess/TEvent.h"
#endif // ROOTCORE

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "xAODTracking/TrackParticleContainer.h"

// Local include(s):
#include "MuonSelectorTools/errorcheck.h"
#include "MuonSelectorTools/MuonSelectionTool.h"

// Needed for Smart Slimming
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"

/// Example of how to run the MuonSelectorTools package to obtain information from muons

int main( int argc, char* argv[] ) {


	// The application's name:
   const char* APP_NAME = argv[ 0 ];

   TString Output = argv[2];

   // Check if we received a file name:
   if( argc < 2 ) {
      Error( APP_NAME, "No file name received!" );
      Error( APP_NAME, "  Usage: %s [xAOD file name]", APP_NAME );
      return 1;
   }

   // Initialise the application:
   CHECK( xAOD::Init( APP_NAME ) );

   // Open the input file:
   const TString fileName = argv[ 1 ];
   Info( APP_NAME, "Opening file: %s", fileName.Data() );
   std::auto_ptr< TFile > ifile( TFile::Open( fileName, "READ" ) );
   CHECK( ifile.get() );

   // Create a TEvent object:
   xAOD::TEvent event;
   CHECK(event.readFrom( ifile.get(), xAOD::TEvent::kClassAccess ));
   Info( APP_NAME, "Number of events in the file: %i",
         static_cast< int >( event.getEntries() ) );

   // Decide how many events to run over:
   
   Long64_t entries = event.getEntries();
   
   if( argc > 3 ) {
      const Long64_t e = atoll( argv[ 2 ] );
      if( e < entries ) {
         entries = e;
      }
   }

   CP::MuonSelectionTool m_muonSelection( "MuonSelection" );

   m_muonSelection.msg().setLevel( MSG::INFO );
   m_muonSelection.setProperty( "MaxEta", 2.7 );
   m_muonSelection.setProperty( "MuQuality", 3);
   //m_muonSelection.setProperty( "Author", 12 );
   //m_muonSelection.initialize();
   m_muonSelection.setProperty( "TurnOffMomCorr", true );
   CHECK (m_muonSelection.initialize().isSuccess());

   int allMuons = 0;
   int badMuons = 0;
   int verylooseMuons = 0;
   int looseMuons = 0;
   int mediumMuons = 0;
   int tightMuons = 0;
   int allgoodMuons = 0;
   int nCaloTagged = 0;

   int nSAMuons = 0;

   int nPositive = 0;
   int nNegative = 0;

   int passesCutsTool = 0;
   int passesCutsxAOD = 0;

   TFile *outfile = new TFile(Output, "recreate");

    //all muons
    TH1D *Zmass_mumu = new TH1D("Zmass_mumu", "m_{Z mumu}; [GeV]; Events", 150, 0, 150);

    TH1D *muon_pt = new TH1D("muon_pt","muon_pt;[GeV]; Events",500,0,500);
    TH1D *muon_eta = new TH1D("muon_eta","muon_eta",60,-6,6);
    TH1D *muon_rapidity = new TH1D("muon_rapidity","muon_rapidity",60,-6,6);
    TH1D *muon_phi = new TH1D("muon_phi","muon_phi",60,-6.28,6.28);
    TH1D *muon_e = new TH1D("muon_e","muon_e",500,0,500);
    TH1D *muon_charge = new TH1D("muon_charge","muon_charge",10,-2,2);

    //very loose muons

    TH1D *muon_very_loose_pt = new TH1D("muon_very_loose_pt","muon_very_loose_pt;[GeV]; Events",500,0,500);
    TH1D *muon_very_loose_eta = new TH1D("muon_very_loose_eta","muon_very_loose_eta",60,-6,6);
    TH1D *muon_very_loose_rapidity = new TH1D("muon_very_loose_rapidity","muon_very_loose_rapidity",60,-6,6);
    TH1D *muon_very_loose_phi = new TH1D("muon_very_loose_phi","muon_very_loose_phi",60,-6.28,6.28);
    TH1D *muon_very_loose_e = new TH1D("muon_very_loose_e","muon_very_loose_e",500,0,500);
    TH1D *muon_very_loose_charge = new TH1D("muon_very_loose_charge","muon_very_loose_charge",10,-2,2);

    //loose muons

    TH1D *muon_loose_pt = new TH1D("muon_loose_pt","muon_loose_pt;[GeV]; Events",500,0,500);
    TH1D *muon_loose_eta = new TH1D("muon_loose_eta","muon_loose_eta",60,-6,6);
    TH1D *muon_loose_rapidity = new TH1D("muon_loose_rapidity","muon_loose_rapidity",60,-6,6);
    TH1D *muon_loose_phi = new TH1D("muon_loose_phi","muon_loose_phi",60,-6.28,6.28);
    TH1D *muon_loose_e = new TH1D("muon_loose_e","muon_loose_e",500,0,500);
    TH1D *muon_loose_charge = new TH1D("muon_loose_charge","muon_loose_charge",10,-2,2);
   
    //medium muons

    TH1D *muon_medium_pt = new TH1D("muon_medium_pt","muon_medium_pt;[GeV]; Events",500,0,500);
    TH1D *muon_medium_eta = new TH1D("muon_medium_eta","muon_medium_eta",60,-6,6);
    TH1D *muon_medium_rapidity = new TH1D("muon_medium_rapidity","muon_medium_rapidity",60,-6,6);
    TH1D *muon_medium_phi = new TH1D("muon_medium_phi","muon_medium_phi",60,-6.28,6.28);
    TH1D *muon_medium_e = new TH1D("muon_medium_e","muon_medium_e",500,0,500);
    TH1D *muon_medium_charge = new TH1D("muon_medium_charge","muon_medium_charge",10,-2,2);
   
    //tight muons
    
    TH1D *muon_tight_pt = new TH1D("muon_tight_pt","muon_tight_pt;[GeV]; Events",500,0,500);
    TH1D *muon_tight_eta = new TH1D("muon_tight_eta","muon_tight_eta",60,-6,6);
    TH1D *muon_tight_rapidity = new TH1D("muon_tight_rapidity","muon_tight_rapidity",60,-6,6);
    TH1D *muon_tight_phi = new TH1D("muon_tight_phi","muon_tight_phi",60,-6.28,6.28);
    TH1D *muon_tight_e = new TH1D("muon_tight_e","muon_tight_e",500,0,500);
    TH1D *muon_tight_charge = new TH1D("muon_tight_charge","muon_tight_charge",10,-2,2);

    //bad muons

    TH1D *muon_bad_pt = new TH1D("muon_bad_pt","muon_bad_pt;[GeV]; Events",500,0,500);
    TH1D *muon_bad_eta = new TH1D("muon_bad_eta","muon_bad_eta",60,-6,6);
    TH1D *muon_bad_rapidity = new TH1D("muon_bad_rapidity","muon_bad_rapidity",60,-6,6);
    TH1D *muon_bad_phi = new TH1D("muon_bad_phi","muon_bad_phi",60,-6.28,6.28);
    TH1D *muon_bad_e = new TH1D("muon_bad_e","muon_bad_e",500,0,500);
    TH1D *muon_bad_charge = new TH1D("muon_bad_charge","muon_bad_charge",10,-2,2);


   // Loop over the events:
   for( Long64_t entry = 0; entry < entries; ++entry ) {

      // Tell the object which entry to look at:
      event.getEntry( entry );

      int goodMuons = 0;
      // Print some event information for fun:
      const xAOD::EventInfo* ei = 0;
      CHECK( event.retrieve( ei, "EventInfo" ) );
      Info( APP_NAME,
            "===>>>  start processing event #%i, "
            "run #%i %i events processed so far  <<<===",
            static_cast< int >( ei->eventNumber() ),
            static_cast< int >( ei->runNumber() ),
            static_cast< int >( entry ) );

      // Get the Muons from the event:
      const xAOD::MuonContainer* muons = 0;
      CHECK( event.retrieve( muons, "Muons" ) );
      Info( APP_NAME, "Number of muons: %i",
            static_cast< int >( muons->size() ) );

      xAOD::Muon::Quality my_quality;
      bool passesIDRequirements = false;
      
      // Print their properties, using the tools:
      xAOD::MuonContainer::const_iterator mu_itr = muons->begin();
      xAOD::MuonContainer::const_iterator mu_end = muons->end();
      //xAOD::MuonContainer::const_iterator * muon0 = 0;
      //xAOD::MuonContainer::const_iterator * muon1 = 0;
      TLorentzVector mu0,mu1;
      float mu_pt0, mu_pt1,mu_phi0, mu_phi1,mu_eta0, mu_eta1,mu_e0, mu_e1;
      float Zmumu_M;
      int k = 0;
      for( ; mu_itr != mu_end; ++mu_itr ) {
		  if (k==0){
	      mu_pt0=std::abs((*mu_itr)->pt())/1000.;
	      mu_phi0=(*mu_itr)->phi();
	      mu_eta0=(*mu_itr)->eta();
	      mu_e0=std::abs((*mu_itr)->e())/1000.;
	      mu0.SetPtEtaPhiE(mu_pt0,mu_eta0,mu_phi0,mu_e0);
		  } 
		  if (k==1){
		  mu_pt1=std::abs((*mu_itr)->pt())/1000.;
		  mu_phi1=(*mu_itr)->phi();
	      mu_eta1=(*mu_itr)->eta();
	      mu_e1=std::abs((*mu_itr)->e())/1000.;
	      mu1.SetPtEtaPhiE(mu_pt1,mu_eta1,mu_phi1,mu_e1);
		  } 
		  if (k>1){
		  Zmumu_M = (mu0+mu1).M();
		  Zmass_mumu -> Fill(Zmumu_M); 
	      }
		  k++;
      //muon0=muon_itr->first;
	  //muon1=muon_itr->first;
		  

	allMuons++;

	uint8_t PixHits = 0, PixDead = 0, SCTHits = 0, SCTDead = 0, PixHoles = 0, SCTHoles = 0, TRTHits = 0, TRTOut = 0;
	uint8_t nprecisionLayers = 0, nprecisionHoleLayers = 0;
	//float momSigBal = 0;
	//float CaloLRLikelihood = 0;
	//int CaloMuonIDTag = 0;

	float abseta = std::abs((*mu_itr)->eta());
	//bool passesTool = false;
	//bool passesxAOD = false;


	Info( APP_NAME, "Muon pT:             %g ", std::abs((*mu_itr)->pt())/1000.);
	Info( APP_NAME, "Muon eta:            %g ", std::abs((*mu_itr)->eta()));
	Info( APP_NAME, "Muon muonType:       %d ", std::abs((*mu_itr)->muonType()));

    // Filling all muons

    muon_pt->Fill(std::abs((*mu_itr)->pt())/1000.);
    muon_eta->Fill(std::abs((*mu_itr)->eta()));
    muon_rapidity->Fill(std::abs((*mu_itr)->rapidity()));
    muon_phi->Fill(std::abs((*mu_itr)->phi()));
    muon_e->Fill(std::abs((*mu_itr)->e())/1000.);
    muon_charge->Fill(std::abs((*mu_itr)->charge()));

	// Info( APP_NAME, "Muon phi:            %g ", std::abs((*mu_itr)->phi()));
	// Info( APP_NAME, "Muon charge:         %g ", (*mu_itr)->charge());
	if((*mu_itr)->charge() > 0)
	  nPositive++;
	else
	  nNegative++;
	// //Info( APP_NAME, "Muon allAuthors:     %d ", std::abs((*mu_itr)->allAuthors()));
	// Info( APP_NAME, "Muon author:         %d ", std::abs((*mu_itr)->author()));
	//Info( APP_NAME, "Muon muonType:       %d ", std::abs((*mu_itr)->muonType()));
	// Info( APP_NAME, "Muon quality:        %d ", std::abs((*mu_itr)->quality()));
	// Info( APP_NAME, "----------------------------------------------------------");
	//	if (!(*mu_itr)->parameter(momSigBal, xAOD::Muon_v1::momentumBalanceSignificance))
	//	  std::cout << "No momSigBal " << std::endl;
	// Info( APP_NAME, "Muon: momSigBal %f ", momSigBal);
	//	  if (!(*mu_itr)->parameter(CaloLRLikelihood, xAOD::Muon_v1::CaloLRLikelihood))
	//	  std::cout << "No caloLRLikelihood " << std::endl;
	//Info( APP_NAME, "Muon: caloLRLH  %f ", CaloLRLikelihood);
	//	    if (!(*mu_itr)->parameter(CaloMuonIDTag, xAOD::Muon_v1::CaloMuonIDTag))
	//	  std::cout << "No caloMuonIDTag " << std::endl;
	//	Info( APP_NAME, "Muon: caloIDTag %u ", CaloMuonIDTag);
	
	
	// if( (*mu_itr)->muonType() == xAOD::Muon_v1::CaloTagged)	{
	//   std::cout << "Found a calo-tagged muon! Yay! With abseta " << abseta << std::endl;
	//   nCaloTagged++;
	// }
        passesIDRequirements = m_muonSelection.passedIDCuts(**mu_itr);
	my_quality = m_muonSelection.getQuality(**mu_itr);	
	if( (*mu_itr)->muonType() == xAOD::Muon_v1::MuonStandAlone)
	  std::cout << "SA MUON " << passesIDRequirements << " " << my_quality << std::endl;
	
	//std::cout << "quality " << my_quality << " IDHits " << passesIDRequirements << std::endl;
	//std::cout << "Comparison of the quality: xAOD vs SelectorTool " << (*mu_itr)->quality() << " " <<  my_quality << std::endl;
	
	
	//std::cout << passesxAOD << " "  << passesTool << std::endl;
	if(abseta < 2.5 && passesIDRequirements && (*mu_itr)->quality() <= xAOD::Muon::Medium){
	  //std:: cout << "Passes the selection (eta, IDCuts and quality) from the xAOD " << std::endl;
	  passesCutsxAOD++; //passesxAOD = true;
	}
	
	if(my_quality <= xAOD::Muon::VeryLoose){
	  verylooseMuons++;
      muon_very_loose_pt->Fill(std::abs((*mu_itr)->pt())/1000.);
      muon_very_loose_eta->Fill(std::abs((*mu_itr)->eta()));
      muon_very_loose_rapidity->Fill(std::abs((*mu_itr)->rapidity()));
      muon_very_loose_phi->Fill(std::abs((*mu_itr)->phi()));
      muon_very_loose_e->Fill(std::abs((*mu_itr)->e())/1000.);
      muon_very_loose_charge->Fill(std::abs((*mu_itr)->charge()));
      }
	if(my_quality <= xAOD::Muon::Loose){
	  looseMuons++;
      muon_loose_pt->Fill(std::abs((*mu_itr)->pt())/1000.);
      muon_loose_eta->Fill(std::abs((*mu_itr)->eta()));
      muon_loose_rapidity->Fill(std::abs((*mu_itr)->rapidity()));
      muon_loose_phi->Fill(std::abs((*mu_itr)->phi()));
      muon_loose_e->Fill(std::abs((*mu_itr)->e())/1000.);
      muon_loose_charge->Fill(std::abs((*mu_itr)->charge()));
      }
	if(my_quality <= xAOD::Muon::Medium){
	  mediumMuons++;
      muon_medium_pt->Fill(std::abs((*mu_itr)->pt())/1000.);
      muon_medium_eta->Fill(std::abs((*mu_itr)->eta()));
      muon_medium_rapidity->Fill(std::abs((*mu_itr)->rapidity()));
      muon_medium_phi->Fill(std::abs((*mu_itr)->phi()));
      muon_medium_e->Fill(std::abs((*mu_itr)->e())/1000.);
      muon_medium_charge->Fill(std::abs((*mu_itr)->charge()));
      }
	if(my_quality <= xAOD::Muon::Tight){
	  tightMuons++;
      muon_tight_pt->Fill(std::abs((*mu_itr)->pt())/1000.);
      muon_tight_eta->Fill(std::abs((*mu_itr)->eta()));
      muon_tight_rapidity->Fill(std::abs((*mu_itr)->rapidity()));
      muon_tight_phi->Fill(std::abs((*mu_itr)->phi()));
      muon_tight_e->Fill(std::abs((*mu_itr)->e())/1000.);
      muon_tight_charge->Fill(std::abs((*mu_itr)->charge()));
      }
	if(my_quality <= xAOD::Muon::VeryLoose && !(my_quality <= xAOD::Muon::Loose)){
	  badMuons++;
      muon_bad_pt->Fill(std::abs((*mu_itr)->pt())/1000.);
      muon_bad_eta->Fill(std::abs((*mu_itr)->eta()));
      muon_bad_rapidity->Fill(std::abs((*mu_itr)->rapidity()));
      muon_bad_phi->Fill(std::abs((*mu_itr)->phi()));
      muon_bad_e->Fill(std::abs((*mu_itr)->e())/1000.);
      muon_bad_charge->Fill(std::abs((*mu_itr)->charge()));
      }
	if(m_muonSelection.accept(**mu_itr)){
	  //std::cout << "FAILED ACCEPT FUNCTION " << std::endl;
	  //std:: cout << "Passes the selection (eta, IDCuts and quality) from the SelectorTool " << std::endl;
	  passesCutsTool++; //passesTool = true;
	}
	
	//std::cout << passesxAOD << " "  << passesTool << std::endl;
	
	// if((passesTool && !passesxAOD) || (!passesTool && passesxAOD)){
	//   std::cout << "DISCREPANCY!!!! " << std::endl;
	//   std::cout << "eta " << abseta << " IDCuts " << passesIDRequirements << std::endl;
	//   std::cout << "Comparison of the quality: xAOD vs SelectorTool " << (*mu_itr)->quality() << " " <<  my_quality << std::endl;
	if( (*mu_itr)->muonType() == xAOD::Muon_v1::Combined)
	  std::cout << "combined " << std::endl;
	else if( (*mu_itr)->muonType() == xAOD::Muon_v1::MuonStandAlone){
	  nSAMuons++;
	  std::cout << "stand-alone " << std::endl;}
	else if( (*mu_itr)->muonType() == xAOD::Muon_v1::SegmentTagged)
	  std::cout << "segment-tagged " << std::endl;
	else if( (*mu_itr)->muonType() == xAOD::Muon_v1::CaloTagged)
	  std::cout << "calo-tagged " << std::endl;
	else if( (*mu_itr)->muonType() == xAOD::Muon_v1::SiliconAssociatedForwardMuon)
	  std::cout << "forward " << std::endl;
	else
	  std::cout << "no type! " << std::endl;

	if (!(*mu_itr)->primaryTrackParticle()->summaryValue(nprecisionLayers, xAOD::numberOfPrecisionLayers))
	  std::cout << "no nprecisionlayers! " << std::endl;
	
	if (!(*mu_itr)->primaryTrackParticle()->summaryValue(nprecisionHoleLayers, xAOD::numberOfPrecisionHoleLayers))
	  std::cout << "no nprecisionholelayers! " << std::endl;
	Info( APP_NAME, "Primary track: NPrecisionLayers %i NPrecisionHoleLayers %i ", nprecisionLayers, nprecisionHoleLayers);
	  // Info( APP_NAME, "Muon: momSigBal %f ", momSigBal);

	// }

	if(!m_muonSelection.accept(**mu_itr)) continue;
	
	if(!(*mu_itr)->primaryTrackParticle()->summaryValue(PixHits,xAOD::numberOfPixelHits))
	  Info( APP_NAME, "Missing info!");
	//Info( APP_NAME, "Primary track: PixHits %i ", PixHits);

	if(!(*mu_itr)->primaryTrackParticle()->summaryValue(PixDead,xAOD::numberOfPixelDeadSensors))
	  Info( APP_NAME, "Missing info!");
	//Info( APP_NAME, "Primary track: PixDead %i ", PixDead);

	if(!(*mu_itr)->primaryTrackParticle()->summaryValue(SCTHits,xAOD::numberOfSCTHits))
	  Info( APP_NAME, "Missing info!");
	//Info( APP_NAME, "Primary track: SCTHits %i ", SCTHits);

	if(!(*mu_itr)->primaryTrackParticle()->summaryValue(SCTDead,xAOD::numberOfSCTDeadSensors))
	  Info( APP_NAME, "Missing info!");
	//Info( APP_NAME, "Primary track: SCTDead %i ", SCTDead);


	if(!(*mu_itr)->primaryTrackParticle()->summaryValue(PixHoles,xAOD::numberOfPixelHoles))
	  Info( APP_NAME, "Missing info!");
	//Info( APP_NAME, "Primary track: PixHoles %i ", PixHoles);

	if(!(*mu_itr)->primaryTrackParticle()->summaryValue(SCTHoles,xAOD::numberOfSCTHoles))
	  Info( APP_NAME, "Missing info!");
	//Info( APP_NAME, "Primary track: SCTHoles %i ", SCTHoles);

	if(!(*mu_itr)->primaryTrackParticle()->summaryValue(TRTHits,xAOD::numberOfTRTHits))
	  Info( APP_NAME, "Missing info!");
	//Info( APP_NAME, "Primary track: TRTHits %i ", TRTHits);

	if(!(*mu_itr)->primaryTrackParticle()->summaryValue(TRTOut,xAOD::numberOfTRTOutliers))
	  Info( APP_NAME, "Missing info!");
	//Info( APP_NAME, "Primary track: TRTOut %i ", TRTOut);

	//uint8_t totTRT = TRTHits+TRTOut;

	//Info( APP_NAME, "Muon eta:  %g ", std::abs((*mu_itr)->eta()));
	//Info( APP_NAME, "TotTRT %i > 5; OutCond %i < %g ", totTRT, TRTOut, 0.9*totTRT);

	// if (!((0.1<abseta && abseta<=1.9 && totTRT>5 && TRTOut<(0.9 * totTRT)) || (abseta <= 0.1 || abseta > 1.9)))
	//   std::cout << "didn't pass the TRT cuts! v1 " << std::endl;

	// if ((0.1<abseta && abseta<=1.9) && !(totTRT>5 && TRTOut<(0.9 * totTRT)))
	//   std::cout << "didn't pass the TRT cuts! v2 " << std::endl;


         // Select "good" muons:
	//	if( ! m_muonSelection.accept( **mu_itr ) ) std::cout << "didn't pass! " << std::endl;

	//	if( (*mu_itr)->quality() == xAOD::Muon_v1::Tight)
	//	  std::cout << "tight " << std::endl;
	//	else if( (*mu_itr)->quality() == xAOD::Muon_v1::Medium)
	//	  std::cout << "medium " << std::endl;
	//	else if( (*mu_itr)->quality() == xAOD::Muon_v1::Loose)
	//	  std::cout << "loose " << std::endl;
	//	else
	//	  std::cout << "no quality! " << std::endl;

	//	if( (*mu_itr)->muonType() == xAOD::Muon_v1::Combined)
	//	  std::cout << "combined " << std::endl;
	//	else if( (*mu_itr)->muonType() == xAOD::Muon_v1::MuonStandAlone)
	//	  std::cout << "stand-alone " << std::endl;
	//	else if( (*mu_itr)->muonType() == xAOD::Muon_v1::SegmentTagged)
	//	  std::cout << "segment-tagged " << std::endl;
	//	else if( (*mu_itr)->muonType() == xAOD::Muon_v1::CaloTagged)
	//	  std::cout << "calo-tagged " << std::endl;
	//	else if( (*mu_itr)->muonType() == xAOD::Muon_v1::SiliconAssociatedForwardMuon)
	//	  std::cout << "forward " << std::endl;
	//	else
	//	  std::cout << "no type! " << std::endl;

	//	Info(APP_NAME, "MyVerboseType %i ", (*mu_itr)->muonType());
	//	Info(APP_NAME, "MyVerboseQuality %i ", (*mu_itr)->quality());
	
	//	Info(APP_NAME, "MyVerboseGetQuality %i ", m_muonSelection.getQuality( **mu_itr ));

	//	if(m_muonSelection.getQuality( **mu_itr ) != xAOD::Muon_v1::Medium) {
	  //	  Info(APP_NAME, "DIDNT PASS THE QUALITY CUTS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	  //continue;
	//	}
	//	if(!(*mu_itr)->primaryTrackParticle()->summaryValue(SCTHoles,xAOD::numberOfSCTHoles))
	// if (!(*mu_itr)->parameter(momSigBal, xAOD::Muon_v1::momentumBalanceSignificance))
	//   std::cout << "No momSigBal " << std::endl;
	
	// Info( APP_NAME, "Muon: momSigBal %f ", momSigBal);

	goodMuons++;
	allgoodMuons++;

         // // Print some info about the selected muon:
         // Info( APP_NAME, "  Selected muon: eta = %g, phi = %g, pt = %g",
         //       ( *mu_itr )->eta(), ( *mu_itr )->phi(), ( *mu_itr )->pt() );
         // Info( APP_NAME, "    Primary track: eta = %g, phi = %g, pt = %g",
         //       ( *mu_itr )->primaryTrackParticle()->eta(),
         //       ( *mu_itr )->primaryTrackParticle()->phi(),
         //       ( *mu_itr )->primaryTrackParticle()->pt() );

         // const xAOD::TrackParticle* idtrack =
         //    ( *mu_itr )->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
         // if( idtrack ) {
         //    Info( APP_NAME, "         ID track: eta = %g, phi = %g, pt = %g",
         //          idtrack->eta(), idtrack->phi(), idtrack->pt() );
         // }

      }

      Info( APP_NAME, "Number of good muons: %i",
	    goodMuons );

      // Close with a message:
      Info( APP_NAME,
            "===>>>  done processing event #%i, "
            "run #%i %i events processed so far  <<<===",
            static_cast< int >( ei->eventNumber() ),
            static_cast< int >( ei->runNumber() ),
            static_cast< int >( entry + 1 ) );
   }

   //END OF THE EVENT LOOP

   outfile->Write();

   Info(APP_NAME, "All muons %i and good muons %i " , allMuons, allgoodMuons);
   
   Info(APP_NAME, "All muons %i, tight %i, medium %i, loose %i, veryloose %i, bad muons %i " , allMuons, tightMuons, mediumMuons, looseMuons, verylooseMuons, badMuons);
   
   Info(APP_NAME, "CaloTagged muons %i Stand-alone muons %i " , nCaloTagged, nSAMuons);

   Info(APP_NAME, "Positive %i and negative muons %i " , nPositive, nNegative);

   Info(APP_NAME, "Good muons xAOD %i and SelectorTool %i " , passesCutsxAOD, passesCutsTool);

   // Needed for Smart Slimming
   xAOD::IOStats::instance().stats().printSmartSlimmingBranchList();
  
   // Return gracefully:
   return 0;


}
